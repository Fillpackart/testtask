﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WpfApplication1.Model
{
    delegate void RemoveHandler(object sender);

    abstract class Detail : INotifyPropertyChanged
    {
        #region fields
        private Command removeCommand;

        protected string DetailName;

        protected double DetailWeight;
        #endregion

        #region properties
        public event RemoveHandler OnRemove;

        public virtual string Name
        {
            get { return DetailName; }
            set
            {
                if (value.Length <= 0)
                    throw new FormatException();
                DetailName = value;
                OnPropertyChanged("Name");
            }
        }

        public virtual double Weight
        {
            get { return DetailWeight; }
            set
            {
                if (value <= 0 || value > 10000)
                    throw new FormatException();
                DetailWeight = value;
                OnPropertyChanged("Weight");
            }
        }
        #endregion

        #region methods
        public Detail(string n, double w)
        {
            DetailName = n;
            DetailWeight = w;
        }

        public Detail(string n)
        {
            DetailName = n;
        }

        public static string DefaultName(Type n)
        {
            if (n == typeof(Car))
                return "новый автомобиль";
            if (n == typeof(Body))
                return "новая рама";
            if (n == typeof(Wheel))
                return "новое колесо";
            if (n == typeof(Door))
                return "новая дверь";
            if (n == typeof(Nut))
                return "новая шайба";
            return "новая деталь";
        }

        public static double DefaultWeight(Type n)
        {

            if (n == typeof(Body))
                return 100;
            if (n == typeof(Wheel))
                return 15.5;
            if (n == typeof(Door))
                return 7.5;
            if (n == typeof(Nut))
                return 0.1;

            return 0;
        }

        public static string CategoryName(Type n)
        {
            if (n == typeof(Wheel))
                return "Колёса";
            if (n == typeof(Door))
                return "Двери";
            if (n == typeof(Body))
                return "Рама";

            throw new Exception("CategoryName принимет только типы: Wheel,Door,Body");
        }

        public virtual void Remove()
        {
            OnRemove?.Invoke(this);
        }

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public Command RemoveCommand
        {
            get
            {
                return removeCommand ??
                (removeCommand = new Command(obj =>
                {
                    Remove();
                }));
            }
        }
        #endregion

        #region events
        public event PropertyChangedEventHandler PropertyChanged;     
        #endregion
    }
}
