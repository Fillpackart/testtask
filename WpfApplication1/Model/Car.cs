﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Data;

namespace WpfApplication1.Model
{
    class Car:Detail
    {
        #region fields
        private string output;

        private int selectedId = 0;

        public Command rotateCommand;

        public Command openCommand;

        public Command weightCommand;
        #endregion

        #region properties
        public ObservableCollection<Detail> CarDetails { get; }

        public CategoryDetail Wheels
        {
            get
            {
                var w = new CategoryDetail(Detail.CategoryName(typeof(Wheel)), this);
                return w;
            }
        }

        public CategoryDetail Doors
        {
            get
            {
                var d = new CategoryDetail(Detail.CategoryName(typeof(Door)), this);
                return d;
            }
        }

        public CategoryDetail Body
        {
            get
            {
                var b = new CategoryDetail(Detail.CategoryName(typeof(Body)), this);
                return b;
            }
        }

        public override double Weight
        {
            get
            {
                DetailWeight = CarDetails.Sum(p => p.Weight);
                foreach (var numerable in CarDetails.OfType<IEnumerable<Detail>>())
                    DetailWeight += numerable.Sum(p => p.Weight);
                return base.DetailWeight;
            }
        }

        public string Output
        {
            private set
            {
                output = value;
                OnPropertyChanged("Output");
            }
            get
            {
                return output;
            }
        }

        public int SelectedId
        {
            get
            {
                return selectedId;
            }
            set
            {
                if (value < 0)
                    throw new FormatException();

                selectedId = value;
                OnPropertyChanged("SelectedId");
            }
        }

        public Command RotateCommand
        {
            get
            {
                return rotateCommand ??
               (rotateCommand = new Command(obj =>
               {
                   Output = "";
                   var rotatebleList = CarDetails.OfType<iRotateble>().ToList();
                   if (rotatebleList.Count > 0)
                       foreach (var r in rotatebleList)
                           r.Move();
                   else
                       Output = "Нет деталей для вращения";
               }));
            }
        }

        public Command OpenCommand
        {
            get
            {
                return openCommand ??
               (openCommand = new Command(obj =>
               {
                   Output = "";
                   var openableList = CarDetails.OfType<iDoor>().Where(p => (p as iOrdinal).Id == SelectedId).ToList();
                   if (openableList.Count > 0)
                       foreach (var r in openableList)
                           r.Open();
                   else
                       Output = $"Деталь не найдена";
               }));
            }
        }

        public Command WeightCommand
        {
            get
            {
                return weightCommand ??
               (weightCommand = new Command(obj =>
               {
                   Output = $"Вес машины: {Weight}кг";

               }));
            }
        }
        #endregion

        #region methods
        public Car(string n,params Detail[] ds) : base(n)
        {
            CarDetails = new ObservableCollection<Detail>();
            foreach (var detail in ds)
            {
                AddDetail(detail);                
            }
        }

        public Car(string n, int w,int d) : base(n)
        {
            CarDetails = new ObservableCollection<Detail>();
            for (int i = 0; i < w + d; i++)
                if (i < w)
                    AddDetail(new Wheel(Detail.DefaultName(typeof(Wheel)), Detail.DefaultWeight(typeof(Wheel))));
                else
                    AddDetail(new Door(Detail.DefaultName(typeof(Door)), Detail.DefaultWeight(typeof(Door))));
            AddDetail(new Body(Detail.DefaultName(typeof(Body)), Detail.DefaultWeight(typeof(Body))));
        }

        public void AddDetail(Detail d)
        {
            d.OnRemove += (sender) => { RemoveDetail(sender as Detail); OnPropertyChanged("Details"); };

            CarDetails.Add(d);

            SetId(d);
            SetRotate(d);
            SetOpen(d);
        }

        public void RemoveDetail(Detail d)
        {
            CarDetails.Remove(d);        
        }

        private void SetRotate(Detail d)
        {
            if (d is iRotateble)
            {

                (d as iRotateble).OnRotate += (sender) =>
                {
                    if (Output != "")
                        Output += "\n";

                    if (d is Body)
                        Output += "Машина едет";
                    else
                        Output += $"Колесо #{(sender as Wheel).Id} вращается";
                };
            }
        }

        private void SetOpen(Detail d)
        {
            if (d is iDoor)
            {
                (d as iDoor).OnOpen += (sender) =>
                {
                    if (Output != "")
                        Output += "\n";

                    if (d is Body)
                        Output += "Увы, это не дверь";
                    else
                    {
                        (d as Door).IsOpen = !(d as Door).IsOpen;
                        Output += (d as Door).IsOpen ? $"Дверь #{(d as Door).Id} открыта" : $"Дверь #{(d as Door).Id} закрыта";
                    }
                };
            }
        }

        private void SetId(Detail d)
        {

            if (d is Wheel)
            {
                (d as iOrdinal).Id = CarDetails.OfType<Wheel>().OfType<iOrdinal>().Max(p => p.Id) + 1;
                (d as iOrdinal).OnNewId += (sender, args) =>
                {
                    var x = CarDetails.OfType<Wheel>().OfType<iOrdinal>().ToList().Find(p => p.Id == (args as Model.IdEventArgs).newId);
                    if (x != null)
                        x.Id = CarDetails.OfType<Wheel>().OfType<iOrdinal>().Max(p => p.Id) + 1;
                };
            }
            if (d is Door)
            {
                (d as iOrdinal).Id = CarDetails.OfType<Door>().OfType<iOrdinal>().Max(p => p.Id) + 1;
                (d as iOrdinal).OnNewId += (sender, args) =>
                {
                    var x = CarDetails.OfType<Door>().OfType<iOrdinal>().ToList().Find(p => p.Id == (args as Model.IdEventArgs).newId);
                    if (x != null)
                        x.Id = CarDetails.OfType<Door>().OfType<iOrdinal>().Max(p => p.Id) + 1;
                };
            }
        }
        #endregion
    }


}
