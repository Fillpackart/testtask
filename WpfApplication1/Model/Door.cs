﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Model
{
    class Door:Detail,iOrdinal,iDoor
    {
       
        private int id;

        private bool isOpen = false;
        public bool IsOpen
        {
            get
            {
                return isOpen;
            }
            set
            {
                isOpen = value;
                OnPropertyChanged("IsOpen");
                OnPropertyChanged("OpenState");
            }
        }
        public string OpenState
        {
            get
            {
                return IsOpen ? "открыта" : "закрыта";
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                if (value <= 0)
                    throw new FormatException();
                OnNewId?.Invoke(this, new IdEventArgs(value));
                id = value;
                OnPropertyChanged("Id");
            }
        }

        
        public Door(string n, double w) : base(n, w)
        {
        }

        public void Open()
        {
           OnOpen(this);
        }


        public event EventHandler OnNewId;

        public event OpenHandler OnOpen;
    }
}
