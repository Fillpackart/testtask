﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Model
{
    interface iDoor
    {
        event OpenHandler OnOpen;
        void Open();
    }

    delegate void OpenHandler(iOrdinal sender);
}
