﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;

namespace WpfApplication1.Model
{
    class CategoryDetail:INotifyPropertyChanged
    {
        #region fields
        private Car Parent;

        private Command addCommand;
        #endregion

        #region properties
        public string Category { get; }

        public ObservableCollection<Detail> Details
        {
            get
            {
                var details = new ObservableCollection<Detail>();

                if (Category == Detail.CategoryName(typeof(Wheel)))
                    details = new ObservableCollection<Detail>(Parent.CarDetails.OfType<Wheel>());
                else if (Category == Detail.CategoryName(typeof(Door)))
                    details = new ObservableCollection<Detail>(Parent.CarDetails.OfType<Door>());
                else if (Category == Detail.CategoryName(typeof(Body)))
                    details = new ObservableCollection<Detail>(Parent.CarDetails.OfType<Body>());

                return details;
            }
        }

        public Command AddCommand
        {
            get
            {
                return addCommand ??
               (addCommand = new Command(obj =>
               {
                   Detail d;
                   if (Category == Detail.CategoryName(typeof(Wheel)))
                       d = new Wheel(Detail.DefaultName(typeof(Wheel)), Detail.DefaultWeight(typeof(Wheel)));
                   else if (Category == Detail.CategoryName(typeof(Door)))
                       d = new Door(Detail.DefaultName(typeof(Door)), Detail.DefaultWeight(typeof(Door)));
                   else if (Category == Detail.CategoryName(typeof(Body)))
                       d = new Body(Detail.DefaultName(typeof(Body)), Detail.DefaultWeight(typeof(Body)));
                   else throw new Exception();

                   Parent.AddDetail(d);
                   d.OnRemove += Remove;

                   OnPropertyChanged("Details");
               },
                (obj) => !(this.Category == Detail.CategoryName(typeof(Body)) & Details.Count > 0)));
            }
        }
        #endregion

        #region methods
        public CategoryDetail(string c, Car parent)
        {
            Category = c;
            Parent = parent;
            foreach (var item in Details)
                item.OnRemove += Remove;
        }

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        private void Remove(object d)
        {
            Parent.RemoveDetail((Detail)d);
            OnPropertyChanged("Details");
        }
        #endregion

        #region events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion    
    }
}
