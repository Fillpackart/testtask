﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Model
{
    interface iOrdinal
    {
        int Id { get; set; }
        event EventHandler OnNewId;
    }
    public class IdEventArgs : EventArgs
    {
        public int newId { get; set; }
        public IdEventArgs(int newid)
        {
            newId = newid;
        }
    }
}
