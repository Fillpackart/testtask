﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Model
{
    class Body:Detail,iRotateble,iOrdinal,iDoor
    {
        #region properties
        public int Id
        {
            get
            {
                return 0;
            }

            set
            {
                if (value <= 0)
                    throw new FormatException();
            }
        }
        #endregion

        #region methods
        public Body(string n, double w) : base(n, w)
        { }

        public void Open()
        {
            OnOpen(this);
        }

        public void Move()
        {
            OnRotate?.Invoke(this);
        }
        #endregion

        #region events
        public event EventHandler OnNewId;

        public event OpenHandler OnOpen;

        public event RotateHandler OnRotate;
        #endregion
    }
}
