﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections;

namespace WpfApplication1.Model
{
    class Wheel : Detail,iOrdinal,iRotateble,IEnumerable<Detail>
    {
        #region fields
        private int id;

        private ObservableCollection<Nut> nuts = new ObservableCollection<Nut>();

        private Command addCommand;
        #endregion

        #region properties
        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                if (value <= 0)
                    throw new FormatException();

                OnNewId?.Invoke(this, new IdEventArgs(value));
                id = value;
                OnPropertyChanged("Id");
            }
        }

        public ObservableCollection<Nut> Nuts
        {
            get
            {
                return nuts;
            }
        }

        public Command AddCommand
        {
            get
            {
                return addCommand ??
               (addCommand = new Command(obj =>
               {
                   AddNut(new Nut(Detail.DefaultName(typeof(Nut)), Detail.DefaultWeight(typeof(Nut))));
               }));
            }
        }
        #endregion

        #region methods
        public Wheel(string n, double w, params Nut[] NutArr) : base(n, w)
        {
            nuts = new ObservableCollection<Nut>();
            if (NutArr.Length == 0)
                for (int i = 0; i < 6; i++)
                    AddNut(new Nut(Detail.DefaultName(typeof(Nut)), Detail.DefaultWeight(typeof(Nut))));
            else
                foreach (var nut in NutArr)
                    AddNut(nut);

        }

        public void AddNut(Nut n)
        {
            nuts.Add(n);
            n.OnRemove += (sender) => { RemoveNut(sender as Nut); };
            OnPropertyChanged("Nuts");
        }

        public void RemoveNut(Nut n)
        {
            nuts.Remove(n);
            OnPropertyChanged("Nuts");
        }

        public void Move()
        {
            OnRotate?.Invoke(this);
        }

        public IEnumerator<Detail> GetEnumerator()
        {
            return Nuts.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Nuts.GetEnumerator();
        }
        #endregion

        #region events
        public event EventHandler OnNewId;

        public event RotateHandler OnRotate;
        #endregion   
    }
   
}
