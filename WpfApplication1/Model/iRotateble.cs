﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Model
{
    interface iRotateble
    {
        event RotateHandler OnRotate;
        void Move();
    }
    delegate void RotateHandler(iRotateble r);
}
