﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.ComponentModel;
using WpfApplication1.Model;

namespace WpfApplication1.ViewModel
{
    class ApplicationViewModel : INotifyPropertyChanged
    {
        #region fields
        private string newName=Detail.DefaultName(typeof(Car));

        private int doorsCount = 4;

        private int wheelsCount = 4;

        private ObservableCollection<Car> cars=new ObservableCollection<Car>();

        private Command addCommand;
        #endregion

        #region properties
        public ObservableCollection<Car> Cars
        {
            get
            {
                return cars;
            }
        }     

        public string NewName
        {
            get
            {
                return newName;
            }
            set
            {

                if (value.Length <= 0)
                    throw new FormatException();

                newName = value;
                OnPropertyChanged("NewName");
            }
        }

        public int DoorsCount
        {
            get
            {
                return doorsCount;
            }
            set
            {
                if (value < 0)
                    throw new FormatException();             

                doorsCount = value;
                OnPropertyChanged("DoorsCount");
            }
        }

        public int WheelsCount
        {
            get
            {
                return wheelsCount;
            }
            set
            {
                if (value < 0)
                    throw new FormatException();

                wheelsCount = value;
                OnPropertyChanged("WheelsCount");
            }
        }

        public Command AddCommand
        {
            get
            {
                return addCommand ??
                (addCommand = new Command(obj =>
                {
                    Car car = new Car(NewName, WheelsCount, DoorsCount);
                    AddCar(car);
                }));
            }
        }
        #endregion

        #region methods
        public ApplicationViewModel()
        {

        }

        public void AddCar(Car c)
        {
            Cars.Insert(0, c);
            c.OnRemove += (sender) => { RemoveCar(c); };
            OnPropertyChanged("Cars");
        }

        public void RemoveCar(Car c)
        {
            Cars.Remove(c);
            OnPropertyChanged("Cars");
        }

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion

        #region events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
